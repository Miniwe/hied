'use strict';

const csv = require('csvtojson')
const _ = require('lodash');
const serieHeight = 70;

class Timeline {
    constructor (query, topics, countries) {

        this.csvFilePath = `./data/${query['source']}.csv`;
        this.countries = (query['countries[]']) ? (_.isArray(query['countries[]'])) ?  query['countries[]'] : [query['countries[]']] : [];
        this.topics = (query['topics[]']) ? (_.isArray(query['topics[]'])) ?  query['topics[]'] : [query['topics[]']] : [];
        this.view = (query['view'] && ['topic', 'country'].indexOf(query['view'])> -1) ? query['view'] : 'topic';

        this.wholeTopics = topics;
        this.wholeCountries = countries;
    }

    _getYear(jsonObj) {
        let year = 1970;
        for (let i = 1984; i < parseInt((new Date).getFullYear()); i++) {
            if (jsonObj[i.toString()] == 'Yes.') {
                year = i;
                return year;
            }
        }
        return year;
    }

    _getCountry(jsonObj) {
        let country = false;
        this.wholeCountries.forEach(item => {
            if (jsonObj[item.code] == 'Yes.') {
                country = item.code;
            }
        });
        return country;
    }

    _getTopics(jsonObj) {
        let topics = [];
        this.wholeTopics.forEach(item => {
            if (jsonObj[item.code] && jsonObj[item.code] == 'Yes.') {
                topics.push(item);
            }
        });
        return (topics.length) ? topics : false;
    }

    _rightRow(jsonObj, seria) {
        let isCountries = true;
        let isTopics = true;
        let isSeria = false;

        if (this.countries.length) {
            isCountries = false;
            this.countries.forEach( item => {
                if (jsonObj[item] == "Yes.") {
                    isCountries = true;
                }
            });
        }

        if (this.topics.length) {
            isTopics = false;
            this.topics.forEach( item => {
                if (jsonObj[item] == "Yes." && _.indexOf(this.topics, item) > -1) {
                    if (this.view == 'topic') {
                        isTopics = (seria.code == item);
                    }
                    else {
                        isTopics = true;
                    }
                }
            });
        }

        if (jsonObj[seria.code] == 'Yes.') {
            isSeria = true;
        }

        let countryCode = this._getCountry(jsonObj);
        if (isSeria && isCountries && isTopics && countryCode) {
            let topics = this._getTopics(jsonObj);
            let pointData = {
              x: Date.UTC(this._getYear(jsonObj), 0 , 1),
              title: ' ',
              topic: (topics.length > 0) ? (_.map(topics, topic => topic.name)).join(', ') : '',
              country: _.find(this.wholeCountries, {code: countryCode}).name,
              caption: jsonObj.title,
              text: jsonObj.text,
              y: seria.pointY,
              shadow: true,
              shape:  'url(/assets/img/icons_black/group.svg)'
              // align: 'right'
            };
            if (this.view == 'topic') {
                pointData.shape = `url(/assets/img/flags/${countryCode.toLowerCase()}.svg)`;
            }
            else {
                if  (topics.length == 1) {
                    pointData.shape = `url(/assets/img/icons_black/${topics.shift().img})`;
                }
            }
            return pointData;
        }
        return false;

    }

    getData() {
        let counter = 0;
        let series = {};
        const boundPoints = [
            {
                x: Date.UTC(1985, 1, 1),
                title: ' ',
                topic: '',
                country: '',
                caption: '',
                text: '',
                y: -1000,
                shadow: true,
                shape:  'url(/assets/img/_.svg)',
                id: 'point_left'
            },
            {
                x: Date.UTC(2018, 1, 1),
                title: ' ',
                topic: '',
                country: '',
                caption: '',
                text: '',
                y: -1000,
                shadow: true,
                shape:  'url(/assets/img/_.svg)',
                id: 'point_right'
            }
        ];

        if (this.view == 'topic') {
            this.wholeTopics.forEach( (item, index) => {
                let seria = {
                  type: 'flags',
                  shape: 'flag',
                  stackDistance : 22,
                  code: item.code,
                  legendIndex: this.wholeTopics.length - index,
                  name: item.name,
                  fillColor: item.fillColor,
                  style: {
                    color: item.color
                  },
                  caption: item.name,
                  icon: "icons_black/" + item.img,
                  pointY: index * serieHeight,
                  data: _.clone(boundPoints)
                };
                series[item.code] = seria;
            });
        }
        else {
            this.wholeCountries.forEach( (item, index) => {
                let seria = {
                    type: 'flags',
                    shape: 'flag',
                    stackDistance : 22,
                    code: item.code,
                    legendIndex: this.wholeCountries.length - index,
                    name: item.name,
                    fillColor: item.colors.fillColor,
                    style: {
                      color: item.colors.color
                    },
                    caption: item.name,
                    icon: "flags/" + item.code.toLowerCase() + ".svg",
                    pointY: index * serieHeight,
                    data: _.clone(boundPoints)
                };
                series[item.code] = seria;
            });
        }

        return new Promise( (resolve, reject) => {
            csv({
                delimiter: ';',
                trim: true
            })
            .fromFile(this.csvFilePath)
            .on('json',(jsonObj, index) => {
                _.each(series, (seria, code) => {
                    let r = this._rightRow(jsonObj, seria);
                    if (r) {
                        seria.data.push(r);
                    }
                });
            }).on('done',() => {
                _.map(series, seria => {
                    let alignedSeriaData = [];
                    seria.data = _.sortBy(seria.data, ['x']);
                    let sGroups = _.groupBy(seria.data, function(pointData) {
                        return (new Date(pointData.x)).getFullYear();
                    });
                    _.each(sGroups, function(group, year){
                        let len = group.length;
                        let oldYear = 0;
                        let diff = 365 / len;
                        group.forEach(function(pointData, index) {
                            pointData.x = pointData.x + index * diff * 86400000;
                            alignedSeriaData.push(pointData);
                        });
                    });
                    seria.data = alignedSeriaData;
                    return seria;
                });
                resolve(series);
            });
        });
    }
}

module.exports = Timeline;
