'use strict';

const serieHeight = 70;

export default class Timechart {

    constructor(holder) {
        this.holder = holder;
        this.countries = [];
        this.topics = [];
        this.source = 'timeline';
        this.view = 'topic';
    }

    setView(view) {
        if (view) {
            this.view = view;
        }
        else {
            return this.view;
        }
    }

    setSource(source) {
        if (source) {
            this.source = source;
        }
        else {
            return this.source;
        }
    }

    setCountries(countries) {
        if (countries) {
            this.countries = countries;
        }
        else {
            return this.countries;
        }
    }

    setTopics(topics) {
        if (topics) {
            this.topics = topics;
        }
        else {
            return this.topics;
        }
    }

    _makePlotBands (series) {
        return _.map(series, (serie, index) => {
            return {
              from: (index) * serieHeight,
              to: (index + 1) * serieHeight,
              color: index % 2 ? 'rgba(63, 81, 181, 0.05)' : '#fff'
            }
        });
    }

    _addRangeInstruction (chart) {
        chart.renderer.text('Select time frame and range here:', 10, 24)
            .css({
                color: '#31708f',
                fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                fontSize: '11.9px'
            })
            .add();
    }

    _updateChart(series) {
        let labelHeight = '56.808px';

        series = _.values(series);
        if (series.length == 15) {
            labelHeight = '59.492px';
        }

        this.chart = Highcharts.StockChart(this.holder, {
            title: {text: ' '},
            followPointer: true,

            chart: {
                marginTop: 50,
            },
            width: 500,
            height: 450,
            ignoreHiddenSeries: false,
            exporting: { enabled: false },
            rangeSelector: {
                buttons: [
                    {type: 'year', count: 1, text: '1y'},
                    {type: 'year', count: 3, text: '3y'},
                    {type: 'year', count: 5, text: '5y'},
                    {type: 'year', count: 10, text: '10y'},
                    {type: 'all', count: 1, text: 'All'}
                ],
                selected: 4,
                inputEnabled: false,
                buttonPosition: {
                    y: 10
                },
            },
            xAxis: [ {
                type: 'datetime',
                minTickInterval: 365 * 24 * 36e5,
                tickmarkPlacement: 'on',
            }, {
                linkedTo:0,
                offset: -50,
                opposite:true
            } ],
            yAxis: {
                gridLineWidth: 0,
                enabled: false,
                title: false,
                lineWidth: 0,
                minorGridLineWidth: 0,
                lineColor: 'transparent',
                name: {enabled: false},
                min: 0,
                max: series.length * serieHeight,
                minorTickLength: 0,
                // top: 90,
                tickLength: 0,
                labels: {
                    enabled: false
                },
                plotBands: this._makePlotBands(series)
            },
            series: series,
            zoomType: 'x',
            plotOptions: {
                series: {
                    pointHeight: serieHeight
                }
            },
            legend: {
                enabled: true,
                layout: 'vertical',
                align: 'left',
                floating: false,
                verticalAlign: 'top',
                x: 0,
                y:  116,
                margin: 0,
                padding: 0,
                symbolHeight: 1,
                symbolPadding: 0,
                symbolRadius: 0,
                symbolWidth: 1,
                itemStyle: {
                    margin: 0,
                    paddingTop: '8px',
                    paddingBottom: '8px',
                    height: labelHeight,
                    overflow: `hidden`,
                    fontWeight: 'normal',
                    whiteSpace: 'normal',
                    display: 'block',
                    minWidth: '190px',
                    fontSize: '13px',
                    borderTop: '1px solid #ffffff',
                    borderBottom: '1px solid #ffffff'
                },
                useHTML: true,
                labelFormatter: function() {
                    return `<img src="/assets/img/${this.options.icon}" alt="" />  ${this.name}`;
                },
                margin: 0
            },

            tooltip: {
                useHTML: true,
                crosshairs: true,
                backgroundColor: 'rgba(247,247,247,0.95)',
                shared: true,
                formatter: function() {
                    let country = this.point.options['country'],
                        topic   = this.point.options['topic'],
                        title   = this.point.options['caption'],
                        text    = this.point.options['text'];
                    return "<b>" + (Highcharts.dateFormat('%Y', new Date(this.x))) + ", "
                            + country + "</b> <br><b>" + title + "</b><div>" + text + " <br> <small class='text-muted'>"
                            + topic + "</small></div>";
                }
            },
            navigator: {
                top: 40
            }
        }, (chart)  => {
            this._addRangeInstruction(chart);
            chart.update({
                legend: {
                    itemStyle: {
                        heigth: labelHeight
                    }
                }
            }, true);
        });

        this.chart.setSize(null, this.chart.series.length * (serieHeight - 1), true);
        setTimeout(()=>{
            if ($(".highcharts-plot-band:first").length) {
                let labelHeight = $(".highcharts-plot-band:first")[0].getBBox().height;
                this.chart.update({
                    legend: {
                        itemStyle: {
                            height: `${labelHeight}px`
                        }
                    }
                })
            }
        }, 400);
    }

    _getData() {
        $.getJSON('/timeline.json', {view: this.view, source: this.source, countries: this.countries, topics: this.topics}, (data) => {
            this._updateChart(data);
        });
    }

    rebiuld() {
        this._getData();
    }
}