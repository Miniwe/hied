'use strict';
module.exports = {
    init() {
        $.material.init();
        $(".navbar-collapse.navbar-responsive-collapse.hidden.collapse").css({
          top: $(".navbar-header").height()
        });
    },

    multiplyTopics(flag, callback) {
        let classFunc = (flag) ? 'addClass' : 'removeClass';
        $("#keys-container a[data-code]")[classFunc]("active");
        callback( _.map($("#keys-container a[data-code].active"), item => $(item).data('code')) );
    },

    initTopics(callback) {
        $("#keys-container a[data-code]").on('click', (event) => {
            event.preventDefault();

            if (event.shiftKey) {
                $(event.currentTarget).toggleClass("active");
            } else {
                $("#keys-container a[data-code]").not(event.currentTarget).removeClass('active');
                $(event.currentTarget).toggleClass("active");
            }
            callback( _.map($("#keys-container a[data-code].active"), item => $(item).data('code')) );
        });
    }
};