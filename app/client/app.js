'use strict';
import inter      from'./inter';
import Timechart  from'./timechart';
import Mapchart   from'./mapchart';

(function($, window, Highcharts, undefined) {

    let timechart, mapchart, debounce;

    let timelineCountries = function (countries) {
        timechart.setCountries(countries);
        debounce();
    };

    let timelineTopics = function (topics) {
        timechart.setTopics(topics);
        debounce();
    };

  return $(function() {

    inter.init();

    if ($('#timeline-container').length) {
        timechart = new Timechart('timeline-container');
        debounce = _.debounce( () => timechart.rebiuld(), 300 );

        window.timechart = timechart;
    }

    $("#data-source").on('change', event => {
        timechart.setSource($(event.target).val());
        debounce();
    });

    if ($('#keys-container').length) {
        inter.initTopics(timelineTopics);

        $(".js-topic-on").on('click', (event) => {
            inter.multiplyTopics(true, timelineTopics);
        });
        $(".js-topic-off").on('click', (event) => {
            inter.multiplyTopics(false, timelineTopics);
        });
    }

    if ($("#map-container").length) {
        $(".js-map-on").on('click', (event) => {
            mapchart && mapchart.selectMultiply(true);
        });
        $(".js-map-off").on('click', (event) => {
            mapchart && mapchart.selectMultiply(false);
        });

        $.get('/countries.json', (data) => {
            mapchart = new Mapchart('map-container', data);
            mapchart.create();
            mapchart.selectCallback(timelineCountries);

            $(".js-map-on").click(); // auto turn on chart by map
        });
    }

    $(".view-select").click((event) => {
        $(".view-select").removeClass('active');
        $(event.currentTarget).addClass('active');
        timechart.setView($(event.currentTarget).data('value'));
        debounce();
    });


  });
})(jQuery, window, window.Highcharts);
