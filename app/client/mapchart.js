'use strict';
export default class Mapchart {

    constructor(holder, data) {
        this.holder = holder;
        this.data = data;
    }

    selectCallback(callback) {
        let chart = this.chart;
        Highcharts.wrap(Highcharts.Point.prototype, 'select', function (proceed) {
            proceed.apply(this, Array.prototype.slice.call(arguments, 1));
            if (chart) {
                callback(_.map(chart.getSelectedPoints(), (item => item.code)));
            }
            else {
                throw new Error('chart not inittialized');
            }
        });
    }

    selectMultiply(flag) {
        this.data.forEach( (country, index) => {
          var point;
          point = this.chart.get(country.code);
            point.select && point.select(flag, true);
        });
    }

    create() {
        this.chart = Highcharts.Map(this.holder, {
            title: false,
            exporting: {
                enabled: false
            },
            mapNavigation: {
                enableMouseWheelZoom: false,
                enabled: true
            },
            series: [
            {
                name: 'Countries',
                data: this.data,
                mapData: window.mapsData['custom/world-robinson-highres'],
                joinBy: ['iso-a2', 'code'],
                nullColor: '#E9F2F9',
                color: '#F26C4F',
                states: {
                    hover: {
                        color: '#1B325F'
                    },
                    select: {
                        color: '#1B325F'
                    }
                },
                allowPointSelect: true,
                animation: {
                    duration: 500
                },
                marker: {
                    enabled: false
                }
            }
            ],
            legend: {
                enabled: false
            },
            chart: {
                events: {
                    load() {
                        this.get("RU").zoomTo();
                        this.mapZoom(0.18, 5400, -9000);
                    }
                }
            }
        });
        return this.chart;
    }
}