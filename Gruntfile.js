'use strict';
let path = require('path');

let config = {
  'tmpPath': '.tmp',
  'logPath': '.logs',
  'server': {
    'port': '5000'
  }
};

module.exports = function(grunt) {
  require('load-grunt-config')(grunt, {
    configPath: path.join(process.cwd(), 'grunt'),
    init: true,
    data: {
      test: false,
      curPath: process.cwd(),
      tmpPath: path.join(process.cwd(), config.tmpPath)
    },
    loadGruntTasks: {
      pattern: 'grunt-*',
      config: require('./package.json'),
      scope: 'devDependencies'
    },
    postProcess: function(config) {},
    preMerge: function(config, data) {}
  });

  require('time-grunt')(grunt);

  grunt.registerTask('tmp:create', 'Create tmp folder', function() {
    grunt.task.run('tmp:delete');
    grunt.file.mkdir(path.join(process.cwd(), config.tmpPath));
    grunt.file.mkdir(path.join(process.cwd(), config.logPath));
  });

  grunt.registerTask('tmp:delete', 'Delete tmp folder', function() {
    grunt.file["delete"](path.join(process.cwd(), config.tmpPath), {
      force: true
    });
  });
};