var should, sut;

sut = require('../app/app.coffee');

should = require('should');

describe('Writing Node with CoffeeScript', function() {
  it('is easy to get started testing... or is it?', function() {
    return true;
  });
  return it('can access exported functions in other modules', function() {
    return sut.greeting('Marcus').should.equal('Hello Marcus!');
  });
});

describe('GET /status', function() {
  return it('respond with json', function(done) {
    return request(app).get('/status').set('Accept', 'application/json').expect('Content-Type', /json/).expect(200, done);
  });
});
