"use strict";

const express = require('express');
const http    = require('http');
const fs      = require('fs');
const _       = require('lodash');
const hbs     = require('express-hbs');
const url     = require('url');

const topics = _.reverse(require('./data/topics.json'));
const countries = _.reverse(_.sortBy(require('./data/countries.json'), ['name']));

hbs.registerHelper('eachByIdx', (context, options) => {
    var output = '';
    var contextSorted = _.reverse(context.concat());
    for (let i=0, j=contextSorted.length; i<j; i++) {
        output += options.fn(contextSorted[i]);
    }
    return output;
});

const Timeline = require('./app/server/timeline');

let app = express();

app.set('port', process.env.PORT || 5000);

app.engine('hbs', hbs.express4({
    partialsDir:   __dirname + '/app/templates/partials',
    defaultLayout: __dirname + '/app/templates/layout/app.hbs',
    layoutsDir:    __dirname + '/app/templates/layout'
}));

app.set('view engine', 'hbs');

app.set('views', __dirname + '/app/templates');

let site_title = 'Higher Education System Dynamics<br>and Institutional Diversity in Post-Soviet Countries';

let common_subtitle = "<small>Higher Education System Dynamics<br>and Institutional Diversity in Post-Soviet Countries</small>";
let common_header_title = 'HiEd';

let pages = {
    about: {
        title: 'About'
    },
    dimensions: {
        title: 'Dimension of Change',
        topics: topics
    },
    countries: {
        title: 'Countries',
        countries: countries
    },
    contributors: {
        title: 'Contributors'
    },
    publications: {
        title: 'Publications'
    },
    contact: {
        title: 'Contact'
    }
};

let params = {
    title: site_title,
    subtitle: common_subtitle,
    header_title: common_header_title,
    pages: function() {
        return _.each(pages, function(page, key) {
            return pages[key].url = "/" + key;
        });
    }
};

app.get('/', function(req, res) {
    res.render('pages/index', _.extend({}, params, {
        subtitle: common_subtitle,
        title: 'Higher Education System Dynamics and Institutional Diversity in Post-Soviet Countries',
        topics: topics
    }));
});

_.each(pages, function(page, key) {
    var pagedata, path;
    path = "/" + key;
    pagedata = _.extend({}, params, page, {isActive: true});
    return app.get(path, function(req, res) {
        res.render("pages/" + key, _.extend({}, params, page));
    });
});

app.get('/countries.json', function(req, res){
    res.json(countries);
});

app.get('/timeline.json', function(req, res){
    let query = url.parse(req.url, true)['query'];
    let timeline = new Timeline(query, topics, countries);
    timeline.getData().then(data => {
        res.json(data);
    });
});

app.get('/status', function(req, res) {
    res.json({
        msg: 'server online'
    });
});

app.use(express["static"](__dirname + '/public'));

app.listen(app.get('port'), function() {
    console.log('info', 'Node app is running at localhost:' + app.get('port'));
});

module.exports = app;
